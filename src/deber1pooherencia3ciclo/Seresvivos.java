/*
 * Elaborar una jerarquía de herencia que modele los seres vivos capaces de hablar. Las clases que deben modelar son: 
loro, niño y adulto. 
Las tres clases deben contener un comportamiento llamado hablar el cual retorna un String con la siguiente salida:
    Hola, me llamo Pedro y se hablar. 
    Soy racional. Tengo 40 años. 
    Nací el 1 de enero de 1965 
    Soy adulto.
Debe realizar las clases considerando que el valor de nombre,  edad, fecha y tipo deben ser propios 
de cada objeto (variables). Además el mensaje debe variar según las siguientes reglas:
    » El loro no sabe su edad y no es racional. 
    » El niño dice su edad basado en su atributo edad (se lo sabe de memoria). 
    » El profesor lo calcula basado en su año de nacimiento.
 */
package deber3poliformismo;
import java.util.ArrayList;
import java.util.Scanner;
public abstract class SeresVivos{
    public String hablar;
    public abstract String hablar(); 
}
class Loro extends SeresVivos{
    String nombre;
    public Loro(String nombre) {
        this.nombre=nombre;
    } 
    @Override
    public String hablar(){
        hablar = nombre;
        return hablar;  
    }      
    @Override
    public String toString(){
        hablar();
        return "Nombre del loro es "+ hablar()+"\nNo es racional\nNo sabe su edad\n▀▀▀▀▀▀▀";
    }
}
class nino extends SeresVivos{
    String nombre;
    int edad;
    public nino(String nombre, int edad) {
        this.nombre=nombre;
        this.edad=edad;
    } 
    @Override
    public String hablar(){
        hablar = nombre+" "+edad;
        return hablar;  
    }      
    @Override
    public String toString(){
        hablar();
        return "Hola, me llamo "+ nombre+" y se hablar\nSoy racional\nTengo "+edad+" años\nSoy niño(a)\n▀▀▀▀▀▀▀";
    }
}
class adulto extends SeresVivos{
    String nombre;
    int dia, mes,anionac; 
    static int edad;
    public adulto(String nombre, int dia, int mes, int anionac) {
        this.nombre=nombre;
        this.dia=dia;
        this.mes=mes;
        this.anionac=anionac;   
    }
    @Override
    public String hablar(){
        hablar = nombre+(2018-anionac);
        return hablar;  
    }      
    @Override
    public String toString(){
        hablar();
        return "Hola, me llamo "+ nombre+" y se hablar\nSoy racional\nTengo "+(2018-anionac)+" anios\nNaci el "+
                dia+"/"+mes+"/"+anionac+"\nSoy Adulto";
    }
}
class TestSeres{
    public static ArrayList <SeresVivos> seres = new ArrayList <SeresVivos> ();
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        String nombre;
        int dia, mes, anio, edad;
        System.out.println("▀▀▀▀▀▀▀DATOS LORO▀▀▀▀▀▀▀");
        System.out.print("Ingrese Nombre del loro: "); nombre = teclado.nextLine();
        SeresVivos Loro = new Loro (nombre);
        System.out.println("▀▀▀▀▀▀▀DATOS NIÑO▀▀▀▀▀▀▀");
        System.out.print("Ingrese nombre: "); nombre = teclado.nextLine();
        System.out.print("Ingresa Edad: "); edad = teclado.nextInt();
        SeresVivos nino = new nino (nombre, edad);
        System.out.println("▀▀▀▀▀▀▀DATOS ADULTO▀▀▀▀▀▀▀");
        System.out.print("Ingrese nombre: "); nombre = teclado.next();
        System.out.println("FECHA DE NACIMIENTO: "); 
        System.out.print("Dia: "); dia = teclado.nextInt();
        System.out.print("Mes: "); mes = teclado.nextInt();
        System.out.print("Año: ");anio = teclado.nextInt();
        System.out.println("▀▀▀▀▀▀▀");
        SeresVivos adulto = new adulto (nombre, dia, mes, anio);
        seres.add(Loro);
        seres.add(nino);
        seres.add(adulto);
        for (SeresVivos ser : seres) {
            System.out.println(ser);
        }
    }
}
